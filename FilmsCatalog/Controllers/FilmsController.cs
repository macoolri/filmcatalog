﻿using FilmsCatalog.Services;
using FilmsCatalog.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FilmsCatalog.Controllers
{
    public class FilmsController : Controller
    {
        private readonly IFilmsService _filmsService;
        public FilmsController(IFilmsService filmsService)
        {
            _filmsService = filmsService;
        }

        
        [HttpGet]
        [Authorize] 
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateFilmViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                await _filmsService.Create(model, User);
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var film = await _filmsService.GetFilm(id);
                return View(film);
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        [Authorize]
        [Route("edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var film = await _filmsService.GetForEdit(id);
                if (film.Author != User.Identity.Name)
                {
                    throw new Exception("You can't edit this movie");
                }
                return View(film);
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        [Route("edit/{id}")]
        public async Task<IActionResult> Edit(EditFilmViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                await _filmsService.Edit(model);
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        [HttpGet]
        [Authorize]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var film = await _filmsService.GetForDelete(id);
                if (film.Author != User.Identity.Name)
                {
                    throw new Exception("You can't delete this movie");
                }
                return View(film);
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(DeleteFilmViewModel model)
        {
            try
            {  
                await _filmsService.Delete(model.Id);
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return RedirectToAction("Create");
            }
        }
    }
}
