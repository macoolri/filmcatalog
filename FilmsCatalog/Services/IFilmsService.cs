﻿using FilmsCatalog.Data;
using FilmsCatalog.Models;
using FilmsCatalog.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FilmsCatalog.Services
{
    public interface IFilmsService
    {
        Task<List<FilmShortViewModel>> GetAllFilms();
        Task Create(CreateFilmViewModel model, ClaimsPrincipal currentUserClaims);
        Task<FilmViewModel> GetFilm(int id);
        Task Edit(EditFilmViewModel model);
        Task<EditFilmViewModel> GetForEdit(int id);
        Task Delete(int id);
        Task<DeleteFilmViewModel> GetForDelete(int id);
    }

    public class FilmsService : IFilmsService
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _environment;
        private readonly UserManager<User> _userManager;
        private static string[] AllowedExtensions { get; set; } = { "jpg", "jpeg", "png" };
        public FilmsService(ApplicationDbContext context,
            IWebHostEnvironment environment, UserManager<User> userManager)
        {
            _context = context;
            _environment = environment;
            _userManager = userManager;
        }

        public async Task<List<FilmShortViewModel>> GetAllFilms()
        {
            var films = await _context.Films.Select(x => new FilmShortViewModel
            {
                Id = x.Id,
                Name = x.Name,
                FilePath = x.FilePath,
                User = x.User
            }).ToListAsync();
            return films;
        }

        public async Task Create(CreateFilmViewModel model, ClaimsPrincipal currentUserClaims)
        {
            var user = await _userManager.GetUserAsync(currentUserClaims);

            var sameBook = await GetByName(model.Name);
            if (sameBook != null)
            {
                throw new ArgumentException($"Film with same name - {model.Name} already exists");
            }
            var fileNameWithPath = await AddPoster(model.File);
           
            await _context.Films.AddAsync(new Film
            {
                Director = model.Director,
                Description = model.Description,
                Year = model.Year,
                Name = model.Name,
                FilePath = fileNameWithPath,
                User = user
            });
            await _context.SaveChangesAsync();
        }

        public async Task<FilmViewModel> GetFilm(int id)
        {
            var film = await Get(id);
            var filmViewModel = new FilmViewModel
            {
                Director = film.Director,
                Description = film.Description,
                Id = film.Id,
                Name = film.Name,
                Year = film.Year,
                FilePath = film.FilePath,
                User = film.User
            };
            return filmViewModel;
        }

        public async Task Edit(EditFilmViewModel model)
        {
            var withSameTitle = await GetByName(model.Name);
            if (withSameTitle != null && withSameTitle.Id != model.Id)
            {
                throw new ArgumentException("You try to edit film and set name from another film");
            }
            var film = await Get(model.Id);
            film.Name = model.Name;
            film.Director = model.Director;
            film.Description = model.Description;
            film.Year = model.Year;
            if (model.File != null)
            {
                var fileNameWithPath = await AddPoster(model.File);
                film.FilePath = fileNameWithPath;
            }
            
            await _context.SaveChangesAsync();
        }

        public async Task<EditFilmViewModel> GetForEdit(int filmId)
        {
            var film = await Get(filmId);
            return new EditFilmViewModel
            {
                Name = film.Name,
                CurrentFilePath = film.FilePath,
                Director = film.Director,
                Description = film.Description,
                Id = filmId,
                Year = film.Year,
                Author = film.User.UserName
            };
        }

        public async Task Delete(int id)
        {
            var film = await Get(id);
            _context.Films.Remove(film);
            await _context.SaveChangesAsync();
        }

        public async Task<DeleteFilmViewModel> GetForDelete(int filmId)
        {
            var film = await Get(filmId);
            return new DeleteFilmViewModel
            {
                Id = film.Id,
                Name = film.Name,
                Author = film.User.UserName
            };
        }

        private async Task<Film> GetByName(string name)
        {
            return await _context.Films.FirstOrDefaultAsync(x => x.Name.ToLower() == name.ToLower());
        }

        private async Task<Film> Get(int id)
        {
            var film = await _context.Films.Include(x => x.User).FirstOrDefaultAsync(x => x.Id == id);
            if (film == null)
            {
                throw new KeyNotFoundException($"Film with Id={id} does not found!");
            }
            return film;
        }

        private async Task<string> AddPoster(IFormFile poster)
        {
            var isFileAttached = poster != null;
            var fileNameWithPath = string.Empty;
            if (isFileAttached)
            {
                var extension = Path.GetExtension(poster.FileName).Replace(".", "");
                if (!AllowedExtensions.Contains(extension))
                {
                    throw new ArgumentException("Attached file has not supported extension");
                }
                fileNameWithPath = $"files/{Guid.NewGuid()}-{poster.FileName}";
                using (var fs = new FileStream(Path.Combine(_environment.WebRootPath, fileNameWithPath), FileMode.Create))
                {
                    await poster.CopyToAsync(fs);
                }
            }

            return fileNameWithPath;
        }
    }
}
