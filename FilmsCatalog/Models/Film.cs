﻿
namespace FilmsCatalog.Models
{
    public class Film
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }
        public User User { get; set; }
        public string FilePath { get; set; }

    }
}
