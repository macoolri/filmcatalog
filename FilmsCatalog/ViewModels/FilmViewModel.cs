﻿using FilmsCatalog.Models;

namespace FilmsCatalog.ViewModels
{
    public class FilmViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public User User { get; set; }
        
    }

    public class FilmShortViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
        public User User { get; set; }
    }
}
