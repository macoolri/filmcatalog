﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.ViewModels
{
    public class CreateFilmViewModel
    {
        [Required(ErrorMessage = "Необходимо заполнить название фильма")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Необходимо заполнить описание фильма")]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Необходимо заполнить год выхода фильма")]
        [Display(Name = "Год выхода фильма")]
        [Range(1600, int.MaxValue, ErrorMessage = "Введите корректный год выхода фильма")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Необходимо заполнить режиссёра фильма")]
        [Display(Name = "Режиссёр")]
        public string Director { get; set; }

        public IFormFile File { get; set; }
    }

    public class EditFilmViewModel : CreateFilmViewModel
    {
        public int Id { get; set; }
        public string CurrentFilePath { get; set; } = "";
        public string Author { get; set; }
    }
    public class DeleteFilmViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
    }
}
