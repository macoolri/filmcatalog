﻿using FilmsCatalog.Models;
using System.Collections.Generic;

namespace FilmsCatalog.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<FilmShortViewModel> Films { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
